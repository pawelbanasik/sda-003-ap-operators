package com.pawelbanasik;

public class Main {

	public static void main(String[] args) {
		int a = 2 * 5 - 3 / 3;
		System.out.println(a);

		boolean sample = false && true;
		System.out.println(sample);

		System.out.println("trueOrFalse?" + (sample == true));
		System.out.println("trueOrFalse?" + (sample = true));

		int i = 4;

		System.out.println("i: " + i++);
		System.out.println("i: " + ++i);
		System.out.println("i: " + --i);
		System.out.println("i: " + i--);
		System.out.println("i: " + i);

		int j = 5;

		boolean sample2 = true || ++j > 5;
		System.out.println("sample2 " + sample2 + "j" + j);

		sample2 = true | ++j > 5;

		System.out.println("sample2 " + sample2 + "j" + j);

	}

}
